var CError = require('../utils/error')

module.exports = {
  redirect: function (req, res, next) {
    if(req.session.adminAuthed) {
      next()
    } else {
      res.redirect('/q1/admin/login')
    }
  },
  auth: function (req, res, next) {
    if(req.session.adminAuthed) {
      next()
    } else {
      next(new CError('not admin', 403))
    }
  }
}