config = require('../config')
handlebars = require 'handlebars'

desc 'cp config files'
task 'config', {async: true}, (params) ->
  agent = process.env.agent or 'yg'
  configs = config.configs
  cmds = configs.map (configPath)->
    paths = configPath.split('/')
    configName = paths[paths.length-1]

    replaceTemplate = "cp -f #{configPath}/#{configName}.#{agent}.js #{configPath}.js && cp -f #{configPath}/#{configName}.#{agent}.coffee #{configPath}.coffee"

  console.log 'config start'

  jake.exec cmds, {printStdout: true}, ->
    console.log '---------------', process.env.NODE_ENV
    console.log cmds
    console.log 'config end'
    complete()



templateStr = '''
upstream {{agent}}_{{projectName}} {
  {{#each urls}}
  server {{host}}:{{port}};
  {{/each}}
}

server {
  listen 80;
  server_name {{domain}};

  root /home/jsb1111/workspace/{{projectName}}_{{agent}};

  location / {
    proxy_pass http://{{agent}}_{{projectName}};#名字和前面的对应，将所有的请求转发给后端的node
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }
}
'''

desc 'generate nginx config'
task 'nginx', {async: true}, (params) ->
  template = handlebars.compile templateStr
  template()
  complete()