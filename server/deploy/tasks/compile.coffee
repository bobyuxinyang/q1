desc 'compile coffee files'
task 'compile', {async: true}, (params) ->
  console.log 'compile start'
  jake.exec 'coffee -c .', {printStdout: true}, ->
    console.log 'compile end'
    complete()
