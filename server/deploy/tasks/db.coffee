desc 'database migration'
task 'migrate', {async: true}, (params) ->
  console.log 'migration start'
  jake.exec "NODE_ENV=#{process.env.NODE_ENV} node node_modules/compound/bin/compound.js db update", {printStdout: true}, ->
    console.log 'migration end'
    complete()
