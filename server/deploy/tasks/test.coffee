request = require 'superagent'
async = require 'async'
_ = require 'lodash'
config = require '../config'

testUrl = (url, code, callback) ->
  request.get(url).end (err, res)->
    if err or res.statusCode isnt code
      errorMsg = "Request: #{url}\nReturn should be 200 but #{res.statusCode}"
      console.log errorMsg
      callback new Error(errorMsg), null
      return

    console.log "Request: #{url}\nReturn #{res.statusCode}"
    callback null, res.statusCode

desc 'Status test'
task 'status-test', {async: true}, (params) ->
  console.log 'status test start'
  agent = process.env.agent
  projectName = config.projectName
  dir = "workspace/#{projectName}_#{agent}"
  servers = config[agent][process.env.NODE_ENV]

  testUrls = []

  for server in servers
    server.dir = dir
    for serverFile in server.serverFiles
      continue unless serverFile.test
      for i in [0...serverFile.instanceCount]
        for t in serverFile.test
          if _.isArray t
            testUrls.push ["#{server.host}:#{serverFile.basePort+i}#{t[0]}", t[1]]
          else
            testUrls.push "#{server.host}:#{serverFile.basePort+i}#{t}"

  setTimeout ->
    async.map testUrls, (item, callback)->
      if _.isArray item
        url = item[0]
        expectedCode = item[1]
      else
        url = item
        expectedCode = 200

      testUrl url, expectedCode, callback
    ,(err, results)->
      throw err if err
      complete()
  , 3000
