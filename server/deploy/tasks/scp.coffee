config = require '../config'

taskCount = 0

desc 'scp then npm install'
task 'scp_one', {async: true}, (params) ->
  agent = process.env.agent or 'yg'
  user = params.user
  host = params.host
  projectName = config.projectName

  scpCmd = "scp #{projectName}_#{agent}.tar.gz #{user}@#{host}:~"
  sshCmd = """ssh #{user}@#{host} 'mkdir -p workspace/#{projectName}_#{agent} \\
    && tar xvzf #{projectName}_#{agent}.tar.gz -C workspace/ \\
    && cd workspace/#{projectName}_#{agent} && npm install || true'"""

  console.log "scp_one task:#{taskCount+1} start"
  jake.exec [scpCmd, sshCmd], {printStdout: true}, ->
    console.log "scp_one task:#{taskCount+1} end"
    taskCount++
    complete()

desc 'scp to servers then npm install'
task 'scp', {async: true}, (params) ->
  console.log 'scp start'
  agent = process.env.agent or 'yg'
  servers = config[agent][process.env.NODE_ENV]
  serverCount = servers.length
  for server in servers
    jake.Task['scp_one'].invoke(server)
  intervalId = setInterval ->
    if taskCount is serverCount
      clearInterval intervalId
      console.log 'scp end'
      complete()
  , 500