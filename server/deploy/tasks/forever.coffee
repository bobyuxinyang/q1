fs = require 'fs'
handlebars = require 'handlebars'

config = require '../config'
nginx_config_dir = "../nginx.config"

forever_cmd_template = handlebars.compile '''
  {{#each .}}
    ssh {{user}}@{{host}} 'cd {{dir}} &&
    {{#each serverFiles}}
      ln -sf {{file}} {{linkName}} &&
      forever stop {{linkName}} || true &&
      {{#each instances}}
      NODE_ENV={{../NODE_ENV}} agent={{../agent}} PORT={{port}} forever start  -al {{../linkName}}_{{port}}.log {{../linkName}} &&
      {{/each}}
    {{/each}}
    true' &&
  {{/each}}
  true
  '''

nginx_config_template = handlebars.compile '''
  upstream {{service.agent}}_{{service.name}} {
    {{#each instances}}
    server {{host}}:{{port}};
    {{/each}}
  }

  server {
    listen 80;
    server_name {{service.domainPrefix}}{{service.domain}};

    root /home/jsb1111/workspace/{{service.projectName}}_{{service.agent}};

    location / {
      proxy_pass http://{{service.agent}}_{{service.name}};#名字和前面的对应，将所有的请求转发给后端的node
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
    }
  }
  '''

static_nginx_config_template = handlebars.compile '''
  server {
    listen {{basePort}};
    server_name {{domainPrefix}}{{domain}};

    root /home/jsb1111/workspace/{{projectName}}_{{agent}}/dist;
    index index.html;
    access_log /var/log/nginx/{{name}}_{{agent}}.access.log;#如果需要日志的话
  }
  '''





runScript = (script, callback) ->
  ex = jake.createExec [script]
  ex.addListener 'error', (msg, code) ->
    if code is 127
      console.log "Couldn't find #{script}!"
    else
      fail 'Fatal error: ' , msg, code

  ex.addListener 'stdout', (msg) ->
    console.log msg.toString('utf-8')

  ex.addListener 'cmdEnd', callback
  ex.run()

desc 'create dir'
directory(nginx_config_dir)


desc 'start forever'
task 'forever-nginx', {async: true}, (options) ->
  unless process.env.agent
    console.log "task forever 缺少必要的参数，请检查一下环境变量是否存在:[agent]"
    throw "task forever 缺少必要的参数[agent]"


  agent = process.env.agent
  projectName = config.projectName
  dir = "workspace/#{projectName}_#{agent}"
  servers = config[agent][process.env.NODE_ENV]

  serviceMap = {}

  for server in servers
    server.dir = dir
    for serverFile in server.serverFiles
      #构建用于填充forever_cmd_template的对象
      serverFile.linkName = "#{agent}_#{serverFile.name}_#{serverFile.file}"
      serverFile.instances = []
      serverFile.projectName = projectName
      serverFile.NODE_ENV = process.env.NODE_ENV
      serverFile.agent = agent


      for i in [0...serverFile.instanceCount]
        serverFile.instances.push 
          port: serverFile.basePort+i 
          host: server.host

      #构建用于填充nginx_config_template的对象
      continue if serverFile.domain is "" or not serverFile.basePort or serverFile.basePort<0

      serviceMap[serverFile.name] ?= {}
      serviceMap[serverFile.name].service ?= serverFile
      serviceMap[serverFile.name].service.domainPrefix = if process.env.NODE_ENV is 'production' then "#{agent}." else "test.#{agent}."
      serviceMap[serverFile.name].instances ?= []
      serviceMap[serverFile.name].instances = serviceMap[serverFile.name].instances.concat serverFile.instances


  #Generate nginx.config files
  jake.Task["#{nginx_config_dir}"].invoke()
  nginxDir = "#{process.env.HOME}/nginx.config/"
  jake.mkdirP nginxDir

  for key, value of serviceMap
    fs.writeFileSync "#{nginx_config_dir}/#{value.service.agent}.#{value.service.domain}.conf", nginx_config_template(value)
    jake.cpR "#{nginx_config_dir}/#{value.service.agent}.#{value.service.domain}.conf", nginxDir
  
  #Forever start
  console.log forever_cmd_template(servers)
  cmd = forever_cmd_template(servers).split("\n").join("")

  jake.exec cmd, {printStdout: true}, ->
    console.log 'task forever end'
    complete()


task 'static-nginx', {async: true}, (options) ->
  unless process.env.agent
    console.log "task forever 缺少必要的参数，请检查一下环境变量是否存在:[agent]"
    throw "task forever 缺少必要的参数[agent]"


  agent = process.env.agent
  projectName = config.projectName
  dir = "workspace/#{projectName}_#{agent}"
  servers = config[agent][process.env.NODE_ENV]

  serviceMap = {}

  for server in servers
    server.dir = dir
    for serverFile in server.serverFiles
      #构建用于填充forever_cmd_template的对象
      serverFile.linkName = "#{agent}_#{serverFile.name}_#{serverFile.file}"
      serverFile.instances = []
      serverFile.projectName = projectName
      serverFile.NODE_ENV = process.env.NODE_ENV
      serverFile.agent = agent
      serverFile.domainPrefix = if process.env.NODE_ENV is 'production' then "#{agent}." else "test.#{agent}."


      for i in [0...serverFile.instanceCount]
        serverFile.instances.push 
          port: serverFile.basePort+i 
          host: server.host

      #构建用于填充nginx_config_template的对象
      continue if serverFile.domain is ""

      serviceMap[serverFile.name] ?= {}
      serviceMap[serverFile.name].service ?= serverFile
      serviceMap[serverFile.name].instances ?= []
      serviceMap[serverFile.name].instances = serviceMap[serverFile.name].instances.concat serverFile.instances


  #Generate nginx.config files
  jake.Task["#{nginx_config_dir}"].invoke()
  nginxDir = "#{process.env.HOME}/nginx.config/"
  jake.mkdirP nginxDir

  for server in servers
    for serverFile in server.serverFiles
      content = static_nginx_config_template(serverFile)
      fs.writeFileSync "#{nginx_config_dir}/#{serverFile.agent}.#{serverFile.domain}_#{server.host}.conf", content
      jake.cpR "#{nginx_config_dir}/#{serverFile.agent}.#{serverFile.domain}_#{server.host}.conf", nginxDir

  for key, value of serviceMap
    content = nginx_config_template(value)
    fs.writeFileSync "#{nginx_config_dir}/#{value.service.agent}.#{value.service.domain}.conf", content
    jake.cpR "#{nginx_config_dir}/#{value.service.agent}.#{value.service.domain}.conf", nginxDir

  complete()