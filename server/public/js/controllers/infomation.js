'use strict';

/**
 * @ngdoc function
 * @name myApp.controller:InfomationCtrl
 * @description
 * # InfomationCtrl
 * Controller of the myApp
 */
angular.module('myApp')
  .controller('InfomationCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    function($rootScope, $scope, $http) {
      var refreshAll = function () {
        $http.get('/q1/infomation/all')
        .success(function (data) {
          $scope.allInfomations = [].concat(data)
        })
      }
      refreshAll()
      
      $scope.viewDetail = function(index) {
        var item = $scope.allInfomations[index]
        $scope.current = {
          _id: item._id,
          title: item.title,
          content: item.content,
          date: item.date
        }        
      }

      $scope.removeItem = function(index) {
        console.log("Current is: ", $scope.current)
        $http.post('/q1/infomation/remove', {id: $scope.current._id})
          .success(function () {
            $scope.current = null
            refreshAll()
          })
      }

      $scope.saveCurrent = function () {
        $http.post('/q1/infomation/update', {
          _id: $scope.current._id,
          title: $scope.current.title,
          content: $scope.current.content
        }).success(function () {
          $scope.current = null
          refreshAll()
        })        
      }

      $scope.addInfomation = function () {
        console.log("add new infomation")
        var newInfo = {
          title: $scope.newInfomationTitle,
          content: "",
          date: new Date()
        }
        $http.post('/q1/infomation', newInfo)
          .success(function () {
            $scope.newInfomationTitle = ""
            refreshAll()    
            toastr.success("更新成功");  
          })
        
      }

  }]);