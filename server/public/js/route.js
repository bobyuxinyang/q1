angular.module('myApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/views/main.html',
        controller: 'MainCtrl'
      })
      .when('/infomation', {
        templateUrl: '/views/infomation.html',
        controller: 'InfomationCtrl'
      })
  });