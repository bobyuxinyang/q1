'use strict';

/**
 * @ngdoc overview
 * @name newbondAdminApp
 * @description
 * # newbondAdminApp
 *
 * Main module of the application.
 */

angular
  .module('myApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'smart-table'
  ]);

var httpErrorHandler = function(resp) {
  toastr.error(resp.data.error);
}

var errorHandler = function(errorMsg) {
  toastr.error(errorMsg); 
}

var successHandler = function(succMsg) {
  toastr.success(succMsg);  
}

