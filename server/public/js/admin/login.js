$(function() {
  $('#loginButton').on('click', function() {
    var password = $('#password').val()
    $.ajax({
      url: './login',
      type: 'POST',
      data: {password: password},
      success: function() {
        location.href = './'
      }
    })
  })
})