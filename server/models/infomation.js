var mongoose = require('mongoose')
var Schema = mongoose.Schema
var async = require('async')
var _ = require('lodash')
var moment = require('moment')
var config = require('../config')

var infomationSchema = new Schema({
  title: {
    type: String,
  },
  content: {
    type: String
  },
  date: {
    type: Date
  }
})

infomationSchema.statics.findAll = function(cb) {
  var Infomation = this || mongoose.model('Infomation')
  Infomation.find({}).exec(cb)
}

Infomation = mongoose.model('Infomation', infomationSchema)
module.exports = Infomation
