var infomation = require('../controllers/infomation')
var adminMiddleware = require('../middlewares/admin')

module.exports = function(router) {
 
  router.get('/q1/infomation/all', adminMiddleware.redirect, infomation.findAll)
  router.post('/q1/infomation', adminMiddleware.redirect, infomation.addNew)
  router.post('/q1/infomation/update', adminMiddleware.redirect, infomation.update)
  router.post('/q1/infomation/remove', adminMiddleware.redirect, infomation.remove)

  router.get('/infomation/all', infomation.findAll)
}