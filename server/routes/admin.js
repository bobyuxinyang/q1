var admin = require('../controllers/admin')
var adminMiddleware = require('../middlewares/admin')

module.exports = function(router) {
  // //admin
  router.get('/q1/admin/login', admin.login)
  router.post('/q1/admin/login', admin.doLogin)
  
  router.get('/q1/admin', adminMiddleware.redirect, admin.index)

}