
var config = require('../config')


var wechatApi = require('wechat-api')
var wechatConfig = config.wechat
console.log("wechat config is: ", wechatConfig)

var redisAccessTokenInMemory = "TOKEN"

//过期时间不准确 提前一点
wechatApi.AccessToken.prototype.isValid = function() {
  return !!this.accessToken && (new Date().getTime()) < (this.expireTime - 3600000)
}

var WechatApiService = new wechatApi(
  wechatConfig.appid, 
  wechatConfig.secret, function(callback) {
    callback(null, redisAccessTokenInMemory)
  }, function(token, callback) {
  redisAccessTokenInMemory = token
  callback()  
})


module.exports = WechatApiService