var util = require('util')

ClientError = function(message, status) {
  status = status ? status : 400
  Error.call(this)
  Error.captureStackTrace(this, this.constructor)
  this.name = 'ClientError'
  this.message = message
  this.status = status
}

util.inherits(ClientError, Error)

module.exports = ClientError