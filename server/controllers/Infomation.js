var async = require('async')
var _ = require('lodash')
var Infomation = require('../models/infomation')
var config = require('../config')


module.exports = {

  addNew: function(req, res, next) {
    var newInfo = new Infomation()
    newInfo.date = req.body.date
    newInfo.title = req.body.title
    newInfo.content = req.body.content
    console.log("newinfo is: ", newInfo)
    newInfo.save(function (err, result) {
      res.send(result)
    })
  },

  remove: function (req, res, next) {
    console.log("REQ is ", req)
    var id = req.body.id
    console.log("remove id is: ", id)
    if (id) {      
      Infomation.remove({_id: id}, function (err) {
        res.send()
      })
    } else {
      res.error()
    }
  },

  update: function (req, res, next) {
    var _id = req.body._id
    var updateData = {} 
    if (req.body.date) {
      updateData.date = req.body.date
    }
    if (req.body.title) {
      updateData.title = req.body.title
    }
    if (req.body.content) {
      updateData.content = req.body.content
    }
    console.log("update ID is: ", _id)
    Infomation.update({
      _id: _id
    }, {
      $set: updateData
    }, function (err, results) {
      console.log("update err: ", err)
      res.send()
    })
  },  

  findAll: function(req, res, next) {
    Infomation.find().exec(function (err, results) {
      res.send(results)
    })
  }

}