var async = require('async')
var _ = require('lodash')
var CError = require('../../utils/error')
var config = require('../../config')

var infomation = require('./infomation')

module.exports = {
  login: function(req, res, next) {
    res.render('admin/login', { title: 'Login' })
  },
  doLogin: function(req, res, next) {
    var pwd = req.body.password
    if (pwd === config.admin.pwd) {
      req.session.adminAuthed = 'authed'
      console.log(req.session.adminAuthed)
      res.send('ok')
    } else {
      next(new CError('密码不对'))
    }
  },
  index: function(req, res, next) {
    res.render('admin/index', {
      title: "NIMABI"
    })    
  },

  infomation: infomation
}