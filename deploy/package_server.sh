#!/bin/bash

PACKAGE_NAME=q1-server.tar.gz

cd ../server
jake package
mv ./pkg/q1.tar.gz ../deploy/pkg/$PACKAGE_NAME
