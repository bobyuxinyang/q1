#!/usr/bin/expect -f

set timeout -1

set REMOTE_IP 121.42.11.68
set REMOTE_LOGIN_NAME root
set REMOTE_LOGIN_PWD iampangxieDEV002
set PACKAGE_NAME q1-server.tar.gz
set REMOTE_PATH /data/app/q1-server

# inner server  xinzhaitong-admin-1
#spawn scp -r ./xuexh5-website.zip freefcw@76.74.178.79:/data/www/xuexh5
spawn scp -r ./pkg/$PACKAGE_NAME $REMOTE_LOGIN_NAME@$REMOTE_IP:$REMOTE_PATH
expect "*assword:*"
send "$REMOTE_LOGIN_PWD\r"
interact

spawn ssh $REMOTE_LOGIN_NAME@$REMOTE_IP
expect "*assword:*"
send "$REMOTE_LOGIN_PWD\r"

send "cd $REMOTE_PATH\r"

send "rm -rf ./dist\r"
send "tar xzvf ./$PACKAGE_NAME\r"
send "cd ./q1 && cnpm install && cd ..\r"

send "mv ./q1 ./dist\r"

send "rm -rf $PACKAGE_NAME\r"
send "exit\r"
interact